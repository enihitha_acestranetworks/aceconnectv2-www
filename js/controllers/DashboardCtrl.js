﻿app.controller('DashboardCtrl', function ($scope,$ionicLoading,SchoolInfo,$state,ajax,$cordovaDialogs,browser) { 
  var School=SchoolInfo.get();
  $scope.image=School.SchoolUrl;
  $scope.id = localStorage.getItem("token"); 
  $scope.placeholder="img/placeholder.png"
  $scope.result = {};  
  $ionicLoading.show({template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner>'}) 
  var obj={ token : $scope.id };
  ajax.post(School.SchoolUrl+'api/aceConnect/Dashboard',obj).then(function(result) { 
    if( result.UserValidation =="Success" )
    {
      $ionicLoading.hide();
      $scope.AnnouncementList = result.AnnouncementList;
      $scope.StudentList = result.StudentList;
      $scope.PrimaryUser =result.StudentList[0].PrimaryUserFirstName;
    }   
  },function(code){
    $ionicLoading.hide(); 
    if(error == 500 || error==404 || error == 0){
      $cordovaDialogs.alert('No (or low) Internet connection. Please check your settings. ,please try after sometime !!', 'AceConnect', 'OK').then(function() {});
    }
  });
  $scope.studentdetails = function(sid){     
    $state.go('menu.studentdetails',{obj:sid});
  }; 
   $scope.openBrowser = function(link) { 
    browser.open(link);
  }
 $scope.colors=[];  for(var i=0; i<10;i++){    $scope.colors[i] = ('#'+ Math.floor(Math.random()*16777215).toString(16));  }
});